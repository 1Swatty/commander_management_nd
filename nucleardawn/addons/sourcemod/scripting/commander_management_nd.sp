/*
---------------------------------------------------------------------------------------------
-File:

commander_management_nd.sp

---------------------------------------------------------------------------------------------
-License:

Nuclear Dawn: Commander Management System
Copyright (C) 2011 - 2012 B.D.A.K. Koch

This program is free software; you can redistribute it and/or modify it under
the terms of the GNU Affero General Public License, version 3.0, as published by the
Free Software Foundation.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
details.

You should have received a copy of the GNU Affero General Public License along with
this program.  If not, see <http://www.gnu.org/licenses/>.

As a special exception, AlliedModders LLC gives you permission to link the
code of this program (as well as its derivative works) to "Half-Life 2," the
"Source Engine," the "SourcePawn JIT," and any Game MODs that run on software
by the Valve Corporation.  You must obey the GNU General Public License in
all respects for all other code used.  Additionally, AlliedModders LLC grants
this exception to all derivative works.  AlliedModders LLC defines further
exceptions, found in LICENSE.txt (as of this writing, version JULY-31-2007),
or <http://www.sourcemod.net/license.php>.

---------------------------------------------------------------------------------------------
-Console Variables:

sm_commander_manager_version PLUGIN_VERSION // [ND] Commander Management System - Version Number (Console flags: Notify and DontRecord)

---------------------------------------------------------------------------------------------
-Admin Commands (default admin flag: 'b'):

sm_set_commander <player> | Promotes player to commander.
sm_demote_commander <ct|consortium|emp|empire> | Demotes the current commander.
sm_commander_ban <player> [<time (minutes)>] | Bans a player from commanding.
sm_commander_unban <player> | Unbans a player from commanding.

---------------------------------------------------------------------------------------------
-Player Commands:

sm_commander_list | Shows a list of players who have currently applied for commander.
sm_commander_bans | Opens a menu showing all commander banned players on the server.

---------------------------------------------------------------------------------------------
-Command Overrides:

mutiny_immunity - Grants immunity to mutiny, if absent admin flag 'b' will be used.

---------------------------------------------------------------------------------------------
-Admin Menu (default admin flag: 'b'):

x. Commander Management
	-	Commander Management:
		1. Set Commander
			-	Select Team:
				1. Consortium
				2. Empire
					[1,2] -	Select Player:
						[1, n]. <List of players>
		2. Demote Commander
			-	Select Commander:
				1. (Consortium) <Commander>
				2. (Empire) <Commander>
		3. Ban from Commander
			-	Select Player:
					[1, n]. <List of players>
					- Select Duration:
						1. this session
						2. 10 minutes
						3. 1 hour
						4. 1 day
						5. 1 week
						6. 1 month
						7. permanently
		4. Unban from Commander
			-	Select Player:
					[1, n]. <List of players>

---------------------------------------------------------------------------------------------
*/

#pragma semicolon 1

// #define PROBLEMS // No, don't do it. 0o

#include <sourcemod>
#include <sdktools>

#undef REQUIRE_PLUGIN
#include <adminmenu>
#define REQUIRE_PLUGIN

#define ND_TEAM_CONSORTIUM 2
#define ND_TEAM_EMPIRE 3

enum SQLType {
	SQLType_SQLITE = 0,
	SQLType_MYSQL
};

#define PLUGIN_NAME "[ND] Commander Management System"
#define PLUGIN_AUTHOR "1Swat2KillThemAll"
#define PLUGIN_DESCRIPTION "Nuclear Dawn: Commander Management System"
#define PLUGIN_VERSION "1.3.0 (GNU/AGPLv3)"
#define PLUGIN_URL "http://forums.alliedmods.net/showthread.php?t=168626"
public Plugin:myinfo = {
	name = PLUGIN_NAME,
	author = PLUGIN_AUTHOR,
	description = PLUGIN_DESCRIPTION,
	version = PLUGIN_VERSION,
	url = PLUGIN_URL
};

new g_LateLoad = true;

new String:g_Tag[32] = "\x04[SM]";

new bool:g_HasAppliedForCommander[MAXPLAYERS+1] = { false, ... };

new g_BanTime[MAXPLAYERS+1] = { -1, ... };

new Handle:g_database = INVALID_HANDLE,
	SQLType:g_SQLType = SQLType_SQLITE;

new Handle:g_adminmenu = INVALID_HANDLE;

new bool:g_use_tr = false;

public OnPluginStart() {
	new Handle:cvar = CreateConVar("sm_commander_management_version", PLUGIN_VERSION, "[ND] Commander Management System - Version Number", FCVAR_NOTIFY | FCVAR_DONTRECORD);
	SetConVarString(cvar, PLUGIN_VERSION);

	RegAdminCmd("sm_set_commander", ConCmd_PromoteToCommander, ADMFLAG_BAN, "sm_set_commander <player> | Promotes player to commander.");
	RegAdminCmd("sm_demote_commander", ConCmd_DemoteCommander, ADMFLAG_BAN, "sm_demote_commander <ct|consortium|emp|empire> | Demotes the current commander.");

	RegAdminCmd("sm_commander_ban", ConCmd_CommanderBan, ADMFLAG_BAN, "sm_commander_ban <player> [<time (minutes)>] | Bans a player from commanding.");
	RegAdminCmd("sm_commander_unban", ConCmd_CommanderUnban, ADMFLAG_BAN, "sm_commander_unban <player> | Unbans a player from commanding.");

	RegConsoleCmd("sm_commander_list", ConCmd_CommanderList, "sm_commander_list | Shows a list of players who have currently applied for commander.");
	RegConsoleCmd("sm_commander_bans", ConCmd_CommanderBans, "sm_commander_bans | Opens a menu showing all commander banned players on the server.");

	AddCommandListener(CmdListener_StartMutiny, "startmutiny");
	AddCommandListener(CmdListener_ApplyForCommander, "applyforcommander");
	AddCommandListener(CmdListener_UnapplyForCommander, "unapplyforcommander"); // I wish I knew of a more elegant way =(

	HookEvent("promoted_to_commander", Event_PromotedToCommander, EventHookMode_Pre);

	InitialiseSQL();

	new Handle:adminmenu = LibraryExists("adminmenu") ? GetAdminTopMenu() : INVALID_HANDLE;
	if (adminmenu != INVALID_HANDLE) {
		OnAdminMenuReady(adminmenu);
	}

	decl String:translation_file[PLATFORM_MAX_PATH];
	BuildPath(Path_SM, translation_file, sizeof(translation_file), "translations/commander_management_nd.phrases.txt");
	if ((g_use_tr = FileExists(translation_file))) {
		LoadTranslations("commander_management_nd.phrases");
	}
	LoadTranslations("common.phrases");

	if (g_LateLoad) {
		for (new i = 1; i <= MaxClients; i++) {
			if (IsClientConnected(i) && IsClientInGame(i) && IsClientAuthorized(i)) {
				OnClientPostAdminCheck(i);
			}
		}

		g_LateLoad = false;
	}
}

public OnLibraryRemoved(const String:name[]) {
	if (StrEqual(name, "adminmenu")) {
		g_adminmenu = INVALID_HANDLE;
	}
}

public OnAdminMenuReady(Handle:topmenu) {
	if (topmenu == g_adminmenu) {
		return;
	}

	new TopMenuObject:category = AddToTopMenu(topmenu, "Commander Management", TopMenuObject_Category, CategoryHandler, INVALID_TOPMENUOBJECT, "commander_management_nd", ADMFLAG_BAN);

	AddToTopMenu(topmenu, "sm_set_commander", TopMenuObject_Item, AdminMenuHandler_SetCommander, category, "sm_set_commander", ADMFLAG_BAN);
	AddToTopMenu(topmenu, "sm_demote_commander", TopMenuObject_Item, AdminMenuHandler_DemoteCommander, category, "sm_demote_commander", ADMFLAG_BAN);
	AddToTopMenu(topmenu, "sm_commander_ban", TopMenuObject_Item, AdminMenuHandler_CommanderBan, category, "sm_ban_commander", ADMFLAG_BAN);
	AddToTopMenu(topmenu, "sm_commander_unban", TopMenuObject_Item, AdminMenuHandler_CommanderUnban, category, "sm_unban_commander", ADMFLAG_BAN);

	g_adminmenu = topmenu;
}

public CategoryHandler(Handle:topmenu, TopMenuAction:action, TopMenuObject:object_id, param, String:buffer[], maxlength) {
	if (action == TopMenuAction_DisplayTitle) {
		TRS_Format(param, g_use_tr, buffer, maxlength, "Commander Management:", "%t:", "Commander Management");
	}
	else if (action == TopMenuAction_DisplayOption) {
		TRS_Format(param, g_use_tr, buffer, maxlength, "Commander Management", "%t", "Commander Management");
	}
}

public OnClientPostAdminCheck(client) {
	CheckCommanderBan(client);
}

public OnClientDisconnect(client) {
	g_HasAppliedForCommander[client] = false;
	g_BanTime[client] = -1;
}

public Action:ConCmd_PromoteToCommander(client, argc) {
	decl String:arg[MAX_NAME_LENGTH];

	if (argc != 1) {
		GetCmdArg(0, arg, sizeof(arg));
		TRS_ReplyToCommand(client, _, g_use_tr, "Usage: %s <player> | Promotes player to commander.", "%t", "Usage: <Command> <player> | Promotes player to commander.", arg);
		return Plugin_Handled;
	}

	GetCmdArg(1, arg, sizeof(arg));

	new target = FindTarget(client, arg, true);

	if (target == -1) {
		return Plugin_Handled;
	}

	if (!IsCommanderBanned(target) && ND_PromoteToCommander(target)) {
		TRS_ShowActivity2(client, "", g_use_tr, "%s %N promoted %N to commander.", "%t", "<Admin> promoted <Player> to commander.", g_Tag, client, target);
	}
	else {
		TRS_ReplyToCommand(client, _, g_use_tr, "%s Unable to promote %N to commander.", "%t", "Unable to promote <Player> to commander.", g_Tag, target);
	}

	return Plugin_Handled;
}

public Action:ConCmd_DemoteCommander(client, argc) {
	decl String:arg[MAX_NAME_LENGTH];

	if (argc != 1) {
		GetCmdArg(0, arg, sizeof(arg));
		TRS_ReplyToCommand(client, _, g_use_tr, "Usage: %s <ct|consortium|emp|empire> | Demotes the current commander.", "%t", "Usage: <Command> <ct|consortium|emp|empire> | Demotes the current commander.", arg);
		return Plugin_Handled;
	}

	GetCmdArg(1, arg, sizeof(arg));

	new team = -1;

	if (StrEqual(arg, "ct", false) || StrEqual(arg, "consortium", false)) {
		team = ND_TEAM_CONSORTIUM;
	}
	else if (StrEqual(arg, "emp", false) || StrEqual(arg, "empire", false)) {
		team = 3;
	}
	else {
		GetCmdArg(0, arg, sizeof(arg));
		TRS_ReplyToCommand(client, _, g_use_tr, "Usage: %s <ct|consortium|emp|empire> | Demotes the current commander.", "%t", "Usage: <Command> <ct|consortium|emp|empire> | Demotes the current commander.", arg);
		return Plugin_Handled;
	}

	new target = ND_DemoteCommander(team);
	if (target != INVALID_ENT_REFERENCE) {
		TRS_ShowActivity2(client, "", g_use_tr, "%s %N demoted %N.", "%t", "<Admin> demoted <Player>.", g_Tag, client, target);
	}
	else {
		TRS_ReplyToCommand(client, _, g_use_tr, "%s Unable to demote commander.", "%t", "Unable to demote commander.", g_Tag);
	}

	return Plugin_Handled;
}

public Action:ConCmd_CommanderBan(client, argc) {
	decl String:arg[MAX_NAME_LENGTH];

	if (argc < 1 || argc > 2) {
		GetCmdArg(0, arg, sizeof(arg));
		TRS_ReplyToCommand(client, _, g_use_tr, "Usage: %s <player> [<time (minutes)>] | Bans a player from commanding.", "%t", "Usage: <Command> <player> [<time (minutes)>] | Bans a player from commanding.", arg);
		return Plugin_Handled;
	}

	GetCmdArg(1, arg, sizeof(arg));

	new target = FindTarget(client, arg, true);

	if (target == -1) {
		return Plugin_Handled;
	}

	new time = -2,
		Float:_time = -2.0;
	if (argc == 2) {
		GetCmdArg(2, arg, sizeof(arg));
		for (new i = 0, j = strlen(arg); i < j; i++) {
			if (arg[i] == ',') {
				arg[i] = '.';
			}

			if (arg[i] == '.') {
				continue;
			}

			if (!IsCharNumeric(arg[i])) {
				TRS_ReplyToCommand(client, _, g_use_tr, "Usage: %s <player> [<time (minutes)>] | Bans a player from commanding.", "%t", "Usage: <Command> <player> [<time (minutes)>] | Bans a player from commanding.", arg);
				return Plugin_Handled;
			}
		}

		_time = StringToFloat(arg);
	}

	if (_time >= 0) {
		time = RoundToNearest(_time * 60.0);
	}

	if (time && time != -2) {
		time += GetTime();
	}

	if (CommanderBan(target, client, time)) {
		TRS_ShowActivity2(client, "", g_use_tr, "%s %N banned %N from being commander.", "%t", "<Admin> banned <Player> from being commander.", g_Tag, client, target);
	}
	else {
		TRS_ReplyToCommand(client, _, g_use_tr, "%s Unable to ban %N from being commander.", "%t", "Unable to ban <Player> from being commander.", g_Tag, target);
	}

	return Plugin_Handled;
}

public Action:ConCmd_CommanderUnban(client, argc) {
	decl String:arg[MAX_NAME_LENGTH];

	if (argc != 1) {
		GetCmdArg(0, arg, sizeof(arg));
		TRS_ReplyToCommand(client, _, g_use_tr, "Usage: %s <player> | Unbans a player from commanding.", "%t", "Usage: <Command> <player> | Unbans a player from commanding.", arg);
		return Plugin_Handled;
	}

	GetCmdArg(1, arg, sizeof(arg));

	new target = FindTarget(client, arg, true);

	if (target == -1) {
		return Plugin_Handled;
	}

	if (CommanderUnban(target, client)) {
		TRS_ShowActivity2(client, "", g_use_tr, "%s %N unbanned %N from being commander.", "%t", "<Admin> unbanned <Player> from being commander.", g_Tag, client, target);
	}
	else {
		TRS_ReplyToCommand(client, _, g_use_tr, "%s Unable to unban %N from being commander.", "%t", "Unable to unban <Player> from being commander.", g_Tag, target);
	}

	return Plugin_Handled;
}

public Action:ConCmd_CommanderList(client, argc) {
	if (!IsClientValid(client)) {
		return Plugin_Handled;
	}

	new team = GetClientTeam(client),
		Handle:menu = CreateMenu(MenuHandler_Generic),
		count = 0;

	if (team != ND_TEAM_CONSORTIUM && team != ND_TEAM_EMPIRE) {
		return Plugin_Handled;
	}

	decl String:display[128];
	new commander = ND_GetCommander(team);
	if (commander != INVALID_ENT_REFERENCE) {
		TRS_Format(client, g_use_tr, display, sizeof(display), "(Commander) %N", "%t", "(Commander) <Player>", commander);
		AddMenuItem(menu, "", display);
	}

	for (new i = 1; i <= MaxClients; i++) {
		if (commander != i && g_HasAppliedForCommander[i] && IsClientConnected(i) && IsClientInGame(i) && GetClientTeam(i) == team) {
			GetClientName(i, display, sizeof(display));
			AddMenuItem(menu, "", display);
			count++;
		}
	}
	TRS_Format(client, g_use_tr, display, sizeof(display), "%i Commander Applicants:", "%t:", "<Number> Commander Applicants", count + _:(commander != INVALID_ENT_REFERENCE));
	SetMenuTitle(menu, display);
	if (!count && commander == INVALID_ENT_REFERENCE) {
		AddMenuItem(menu, "", "No applicants!", ITEMDRAW_DISABLED);
	}
	SetMenuExitButton(menu, true);

	DisplayMenu(menu, client, MENU_TIME_FOREVER);

	return Plugin_Handled;
}

public Action:ConCmd_CommanderBans(client, argc) {
	if (!IsClientValid(client)) {
		return Plugin_Handled;
	}

	new Handle:menu = CreateMenu(MenuHandler_Generic),
		count = 0;

	decl String:display[128],
		String:duration[16];

	for (new i = 1; i <= MaxClients; i++) {
		if (IsCommanderBanned(i) && IsClientConnected(i) && IsClientInGame(i)) {
			if (g_BanTime[i] > 0) {
				TRS_Format(client, g_use_tr, duration, sizeof(duration), "%.1f minute(s)", "%t", "<Number> minute(s)", (g_BanTime[i] - GetTime()) / 60.0);
			}
			else {
				TRS_Format(client, g_use_tr, duration, sizeof(duration), g_BanTime[i] == 0 ? "permanent" : "current session", "%t", g_BanTime[i] == 0 ? "permanent" : "current session");
			}

			Format(display, sizeof(display), "%N - %s", i, duration);
			AddMenuItem(menu, "", display);
			count++;
		}
	}

	TRS_Format(client, g_use_tr, display, sizeof(display), "Commander Bans:", "%t:", "Commander Bans");
	SetMenuTitle(menu, display, count);
	if (!count) {
		TRS_Format(client, g_use_tr, display, sizeof(display), "No bans!", "%t", "No bans!");
		AddMenuItem(menu, "", display, ITEMDRAW_DISABLED);
	}
	SetMenuExitButton(menu, true);

	DisplayMenu(menu, client, MENU_TIME_FOREVER);

	return Plugin_Handled;
}

public AdminMenuHandler_SetCommander(Handle:topmenu, TopMenuAction:action, TopMenuObject:object_id, param, String:buffer[], maxlength) {
	if (action == TopMenuAction_DisplayOption) {
		TRS_Format(param, g_use_tr, buffer, maxlength, "Set Commander", "%t", "Set Commander");
	}
	else if (action == TopMenuAction_SelectOption) {
		SetCommanderMenu_Team(param);
	}
}

SetCommanderMenu_Team(client) {
	new Handle:menu = CreateMenu(MenuHandler_SetCommander_Team);

	if (menu != INVALID_HANDLE) {
		decl String:display[128];
		TRS_Format(client, g_use_tr, display, sizeof(display), "Select Team:", "%t:", "Select Team");
		SetMenuTitle(menu, display);
		TRS_Format(client, g_use_tr, display, sizeof(display), "Consortium", "%t", "Consortium");
		AddMenuItem(menu, "ct", display);
		TRS_Format(client, g_use_tr, display, sizeof(display), "Empire", "%t", "Empire");
		AddMenuItem(menu, "emp", display);
		AddMenuItem(menu, "", "", ITEMDRAW_SPACER);
		TRS_Format(client, g_use_tr, display, sizeof(display), "Cancel", "%t", "Cancel");
		AddMenuItem(menu, "cancel", display);
		SetMenuExitButton(menu, true);

		DisplayMenu(menu, client, MENU_TIME_FOREVER);
	}
}

public MenuHandler_SetCommander_Team(Handle:menu, MenuAction:action, param1, param2) {
	if (action == MenuAction_Select) {
		decl String:info[32];
		GetMenuItem(menu, param2, info, sizeof(info));

		if (StrEqual(info, "ct")) {
			SetCommanderMenu_Player(param1, ND_TEAM_CONSORTIUM);
		}
		else if (StrEqual(info, "emp")) {
			SetCommanderMenu_Player(param1, ND_TEAM_EMPIRE);
		}
		else if (StrEqual(info, "cancel")) {
			RedisplayAdminMenu(g_adminmenu, param1);
		}
	}
	else if (action == MenuAction_End) {
		CloseHandle(menu);
	}
}

SetCommanderMenu_Player(client, team) {
	new Handle:menu = CreateMenu(MenuHandler_SetCommander_Player);

	if (menu != INVALID_HANDLE) {
		new count = 0,
			commander = ND_GetCommander(team);

		decl String:info[32],
			String:display[128];

		if (commander != INVALID_ENT_REFERENCE) {
			TRS_Format(client, g_use_tr, display, sizeof(display), "(Commander) %N", "%t", "(Commander) <Player>", commander, ITEMDRAW_DISABLED);
			AddMenuItem(menu, "", display);
		}

		for (new i = 1; i <= MaxClients; i++) {
			if (i != commander && IsClientValid(i) && GetClientTeam(i) == team) {
				Format(info, sizeof(info), "%i|%i", GetClientUserId(i), team);
				Format(display, sizeof(display), "%N", i);
				AddMenuItem(menu, info, display);
				count++;
			}
		}

		if (!count) {
			TRS_Format(client, g_use_tr, display, sizeof(display), "No players!", "%t", "No players!");
			AddMenuItem(menu, "", display, ITEMDRAW_DISABLED);
		}

		TRS_Format(client, g_use_tr, display, sizeof(display), "Select Player:", "%t:", "Select Player");
		SetMenuTitle(menu, display);
		AddMenuItem(menu, "", "", ITEMDRAW_SPACER);
		TRS_Format(client, g_use_tr, display, sizeof(display), "Cancel", "%t", "Cancel");
		AddMenuItem(menu, "cancel", display);
		SetMenuExitButton(menu, true);

		DisplayMenu(menu, client, MENU_TIME_FOREVER);
	}
}

public MenuHandler_SetCommander_Player(Handle:menu, MenuAction:action, param1, param2) {
	if (action == MenuAction_Select) {
		decl String:info[32];
		GetMenuItem(menu, param2, info, sizeof(info));

		if (StrEqual(info, "") || StrEqual(info, "cancel")) {
			SetCommanderMenu_Team(param1);
		}
		else {
			decl String:data[2][16];

			ExplodeString(info, "|", data, sizeof(data), sizeof(data[]));
			new target = GetClientOfUserId(StringToInt(data[0])),
				team = StringToInt(data[1]);

			if (!IsClientValid(target)) {
				TRS_PrintToChat(param1, _, g_use_tr, "%s That player is no longer available!", "%t", "That player is no longer available!", g_Tag);
			}
			else if (GetClientTeam(target) != team) {
				TRS_PrintToChat(param1, _, g_use_tr, "%s %N has switched teams!", "%t", "<Player> has switched teams!", g_Tag, target);
			}
			else if (IsCommanderBanned(target)) {
				TRS_PrintToChat(param1, _, g_use_tr, "%s %N is banned from commanding!", "%t", "<Player> is banned from commanding!", g_Tag, target);
			}
			else if (!CanUserTarget(param1, target)) {
				TRS_PrintToChat(param1, _, g_use_tr, "%s You cannot target %N!", "%t", "You cannot target <Player>!", g_Tag, target);
			}
			else if (ND_PromoteToCommander(target)) {
				TRS_ShowActivity2(param1, "", g_use_tr, "%s %N promoted %N to commander.", "%t", "<Admin> promoted <Player> to commander.", g_Tag, param1, target);
			}
			else {
				TRS_PrintToChat(param1, _, g_use_tr, "%s Unable to promote %N to commander.", "%t", "Unable to promote <Player> to commander.", g_Tag, target);
			}

			RedisplayAdminMenu(g_adminmenu, param1);
		}
	}
	else if (action == MenuAction_End) {
		CloseHandle(menu);
	}
}

public AdminMenuHandler_DemoteCommander(Handle:topmenu, TopMenuAction:action, TopMenuObject:object_id, param, String:buffer[], maxlength) {
	if (action == TopMenuAction_DisplayOption) {
		TRS_Format(param, g_use_tr, buffer, maxlength, "Demote Commander", "%t", "Demote Commander");
	}
	else if (action == TopMenuAction_SelectOption) {
		DemoteCommanderMenu(param);
	}
}

DemoteCommanderMenu(client) {
	new Handle:menu = CreateMenu(MenuHandler_DemoteCommander);

	if (menu != INVALID_HANDLE) {
		new ct = ND_GetCommander(ND_TEAM_CONSORTIUM),
			emp = ND_GetCommander(ND_TEAM_EMPIRE);

		decl String:info[32],
			String:display[128];

		if (ct != INVALID_ENT_REFERENCE) {
			Format(info, sizeof(info), "%i|2", GetClientUserId(ct));
			TRS_Format(client, g_use_tr, display, sizeof(display), "(Consortium) %N", "%t", "(Consortium) <Player>", ct);
			AddMenuItem(menu, info, display);
		}
		else {
			TRS_Format(client, g_use_tr, display, sizeof(display), "(Consortium) NO COMMANDER", "%t", "(Consortium) NO COMMANDER");
			AddMenuItem(menu, "", display, ITEMDRAW_DISABLED);
		}

		if (emp != INVALID_ENT_REFERENCE) {
			Format(info, sizeof(info), "%i|3", GetClientUserId(emp));
			TRS_Format(client, g_use_tr, display, sizeof(display), "(Empire) %N", "%t", "(Empire) <Player>", emp);
			AddMenuItem(menu, info, display);
		}
		else {
			TRS_Format(client, g_use_tr, display, sizeof(display), "(Empire) NO COMMANDER", "%t", "(Empire) NO COMMANDER");
			AddMenuItem(menu, "", display, ITEMDRAW_DISABLED);
		}

		SetMenuTitle(menu, "Select Commander:");
		AddMenuItem(menu, "", "", ITEMDRAW_SPACER);
		AddMenuItem(menu, "cancel", "Cancel");
		SetMenuExitButton(menu, true);

		DisplayMenu(menu, client, MENU_TIME_FOREVER);
	}
}

public MenuHandler_DemoteCommander(Handle:menu, MenuAction:action, param1, param2) {
	if (action == MenuAction_Select) {
		decl String:info[32];
		GetMenuItem(menu, param2, info, sizeof(info));

		if (StrEqual(info, "") || StrEqual(info, "cancel")) {
			RedisplayAdminMenu(g_adminmenu, param1);
			return;
		}

		decl String:data[2][16];

		ExplodeString(info, "|", data, sizeof(data), sizeof(data[]));
		new target = GetClientOfUserId(StringToInt(data[0])),
			team = StringToInt(data[1]);

		if (!IsClientValid(target)) {
			TRS_PrintToChat(param1, _, g_use_tr, "%s That player is no longer available!", "%t", "That player is no longer available!", g_Tag);
		}
		else if (target != ND_GetCommander(team)) {
			TRS_PrintToChat(param1, _, g_use_tr, "%s %N is no longer that team's commander!", "%t", "<Player> is no longer that team's commander!", g_Tag, target);
		}
		else if (!CanUserTarget(param1, target)) {
			TRS_PrintToChat(param1, _, g_use_tr, "%s You cannot target %N!", "%t", "You cannot target <Player>!", g_Tag, target);
		}
		else if (ND_DemoteCommander(team) != INVALID_ENT_REFERENCE) {
			TRS_ShowActivity2(param1, "", g_use_tr, "%s %N demoted %N.", "%t", "<Admin> demoted <Player>.", g_Tag, param1, target);
		}
		else {
			TRS_PrintToChat(param1, _, g_use_tr, "%s Unable to demote commander.", "%t", "Unable to demote commander.", g_Tag);
		}

		RedisplayAdminMenu(g_adminmenu, param1);
	}
	else if (action == MenuAction_End) {
		CloseHandle(menu);
	}
}

public AdminMenuHandler_CommanderBan(Handle:topmenu, TopMenuAction:action, TopMenuObject:object_id, param, String:buffer[], maxlength) {
	if (action == TopMenuAction_DisplayOption) {
		TRS_Format(param, g_use_tr, buffer, maxlength, "Ban from Commander", "%t", "Ban from Commander");
	}
	else if (action == TopMenuAction_SelectOption) {
		CommanderBanMenu_Player(param);
	}
}

CommanderBanMenu_Player(client) {
	new Handle:menu = CreateMenu(MenuHandler_CommanderBan_Player);

	if (menu != INVALID_HANDLE) {
		new count = 0;

		decl String:info[32],
			String:display[128];

		for (new i = 1; i <= MaxClients; i++) {
			if (!IsCommanderBanned(i) && IsClientValid(i)) {
				Format(info, sizeof(info), "%i", GetClientUserId(i));
				GetClientName(i, display, sizeof(display));
				AddMenuItem(menu, info, display);
				count++;
			}
		}

		if (!count) {
			TRS_Format(client, g_use_tr, display, sizeof(display), "No players!", "%t", "No players!");
			AddMenuItem(menu, "", display, ITEMDRAW_DISABLED);
		}

		TRS_Format(client, g_use_tr, display, sizeof(display), "Select Player:", "%t:", "Select Player");
		SetMenuTitle(menu, display);
		AddMenuItem(menu, "", "", ITEMDRAW_SPACER);
		TRS_Format(client, g_use_tr, display, sizeof(display), "Cancel", "%t", "Cancel");
		AddMenuItem(menu, "cancel", display);
		SetMenuExitButton(menu, true);

		DisplayMenu(menu, client, MENU_TIME_FOREVER);
	}
}

public MenuHandler_CommanderBan_Player(Handle:menu, MenuAction:action, param1, param2) {
	if (action == MenuAction_Select) {
		decl String:info[32];
		GetMenuItem(menu, param2, info, sizeof(info));

		if (StrEqual(info, "") || StrEqual(info, "cancel")) {
			RedisplayAdminMenu(g_adminmenu, param1);
			return;
		}

		new target = GetClientOfUserId(StringToInt(info));

		if (!IsClientValid(target)) {
			TRS_PrintToChat(param1, _, g_use_tr, "%s That player is no longer available!", "%t", "That player is no longer available!", g_Tag);
		}
		else {
			CommanderBanMenu_Time(param1, target);
		}
	}
	else if (action == MenuAction_End) {
		CloseHandle(menu);
	}
}

CommanderBanMenu_Time(client, target) {
	new Handle:menu = CreateMenu(MenuHandler_CommanderBan_Time);

	if (menu != INVALID_HANDLE) {
		decl String:info[32],
			String:display[128];

		static const String:times_display[7][] = {
			"this session",
			"10 minutes",
			"1 hour",
			"1 day",
			"1 week",
			"1 month",
			"permanently"
		};
		static const times[7] = {
			-2,
			600,
			3600,
			86400,
			604800,
			2592000,
			0
		};

		for (new i = 0; i < 7; i++) {
			Format(info, sizeof(info), "%i|%i", GetClientUserId(target), times[i]);
			TRS_Format(client, g_use_tr, display, sizeof(display), times_display[i], "%t", times_display[i]);
			AddMenuItem(menu, info, display);
		}

		TRS_Format(client, g_use_tr, display, sizeof(display), "Select Duration:", "%t:", "Select Duration");
		SetMenuTitle(menu, "Select Duration:");
		AddMenuItem(menu, "", "", ITEMDRAW_SPACER);
		TRS_Format(client, g_use_tr, display, sizeof(display), "Cancel", "%t", "Cancel");
		AddMenuItem(menu, "cancel", display);
		SetMenuExitButton(menu, true);

		DisplayMenu(menu, client, MENU_TIME_FOREVER);
	}
}

public MenuHandler_CommanderBan_Time(Handle:menu, MenuAction:action, param1, param2) {
	if (action == MenuAction_Select) {
		decl String:info[32];
		GetMenuItem(menu, param2, info, sizeof(info));

		if (StrEqual(info, "") || StrEqual(info, "cancel")) {
			RedisplayAdminMenu(g_adminmenu, param1);
			return;
		}

		decl String:data[2][16];

		ExplodeString(info, "|", data, sizeof(data), sizeof(data[]));
		new target = GetClientOfUserId(StringToInt(data[0])),
			time = StringToInt(data[1]);

		if (time > 0) {
			time += GetTime();
		}

		if (!IsClientValid(target)) {
			TRS_PrintToChat(param1, _, g_use_tr, "%s That player is no longer available!", "%t", "That player is no longer available!", g_Tag);
		}
		else if (CommanderBan(target, param1, time)) {
			TRS_ShowActivity2(param1, "", g_use_tr, "%s %N banned %N from being commander.", "%t", "<Admin> banned <Player> from being commander.", g_Tag, param1, target);
		}
		else {
			TRS_PrintToChat(param1, _, g_use_tr, "%s Unable to ban %N from being commander.", "%t", "Unable to ban <Player> from being commander.", g_Tag, target);
		}

		RedisplayAdminMenu(g_adminmenu, param1);
	}
	else if (action == MenuAction_End) {
		CloseHandle(menu);
	}
}


public AdminMenuHandler_CommanderUnban(Handle:topmenu, TopMenuAction:action, TopMenuObject:object_id, param, String:buffer[], maxlength) {
	if (action == TopMenuAction_DisplayOption) {
		TRS_Format(param, g_use_tr, buffer, maxlength, "Unban from Commander", "%t", "Unban from Commander");
	}
	else if (action == TopMenuAction_SelectOption) {
		CommanderUnbanMenu(param);
	}
}

CommanderUnbanMenu(client) {
	new Handle:menu = CreateMenu(MenuHandler_CommanderUnban);

	if (menu != INVALID_HANDLE) {
		new count = 0;

		decl String:info[32],
			String:display[128],
			String:duration[16];

		for (new i = 1; i <= MaxClients; i++) {
			if (IsCommanderBanned(i) && IsClientValid(i)) {
				if (g_BanTime[i] > 0) {
					Format(duration, sizeof(duration), "%.1f minute(s)", (g_BanTime[i] - GetTime()) / 60.0);
				}
				else {
					Format(duration, sizeof(duration), g_BanTime[i] == 0 ? "permanent" : "current session");
				}

				Format(info, sizeof(info), "%i", GetClientUserId(i));
				Format(display, sizeof(display), "%N - %s", i, duration);
				AddMenuItem(menu, info, display);
				count++;
			}
		}

		if (!count) {
			TRS_Format(client, g_use_tr, display, sizeof(display), "No players!", "%t", "No players!");
			AddMenuItem(menu, "", display, ITEMDRAW_DISABLED);
		}

		TRS_Format(client, g_use_tr, display, sizeof(display), "Select Player:", "%t:", "Select Player");
		SetMenuTitle(menu, display);
		AddMenuItem(menu, "", "", ITEMDRAW_SPACER);
		TRS_Format(client, g_use_tr, display, sizeof(display), "Cancel", "%t", "Cancel");
		AddMenuItem(menu, "cancel", display);
		SetMenuExitButton(menu, true);

		DisplayMenu(menu, client, MENU_TIME_FOREVER);
	}
}

public MenuHandler_CommanderUnban(Handle:menu, MenuAction:action, param1, param2) {
	if (action == MenuAction_Select) {
		decl String:info[32];
		GetMenuItem(menu, param2, info, sizeof(info));

		if (StrEqual(info, "") || StrEqual(info, "cancel")) {
			RedisplayAdminMenu(g_adminmenu, param1);
			return;
		}

		new target = GetClientOfUserId(StringToInt(info));

		if (!IsClientValid(target)) {
			TRS_PrintToChat(param1, _, g_use_tr, "%s That player is no longer available!", "%t", "That player is no longer available!", g_Tag);
		}
		else if (CommanderUnban(target, param1)) {
			TRS_ShowActivity2(param1, "", g_use_tr, "%s %N unbanned %N from being commander.", "%t", "<Admin> unbanned <Player> from being commander.", g_Tag, param1, target);
		}
		else {
			TRS_PrintToChat(param1, _, g_use_tr, "%s Unable to unban %N from being commander.", "%t", "Unable to unban <Player> from being commander.", g_Tag, target);
		}

		RedisplayAdminMenu(g_adminmenu, param1);
	}
	else if (action == MenuAction_End) {
		CloseHandle(menu);
	}
}

public Action:CmdListener_StartMutiny(client, const String:command[], argc) {
	if (!IsClientValid(client)) {
		return Plugin_Continue;
	}

	new team = GetClientTeam(client);
	if (team != ND_TEAM_CONSORTIUM && team != ND_TEAM_EMPIRE) {
		return Plugin_Continue;
	}

	new commander = ND_GetCommander(team);

	if (commander == INVALID_ENT_REFERENCE || client == commander || !CheckCommandAccess(commander, "mutiny_immunity", ADMFLAG_BAN)) {
		return Plugin_Continue;
	}

	TRS_PrintToChat(client, _, g_use_tr, "%s %N is immune to mutiny.", "%t", "<Player> is immune to mutiny.", g_Tag, commander);

	return Plugin_Handled;
}

public Action:CmdListener_ApplyForCommander(client, const String:command[], argc) {
	if (IsClientValid(client) && IsCommanderBanned(client)) {
		TRS_PrintToChat(client, _, g_use_tr, "%s You're banned from playing as commander.", "%t", "You're banned from playing as commander.", g_Tag);
		return Plugin_Handled;
	}

	g_HasAppliedForCommander[client] = true;
	return Plugin_Continue;
}

public Action:CmdListener_UnapplyForCommander(client, const String:command[], argc) {
	g_HasAppliedForCommander[client] = false;
	return Plugin_Continue;
}

public Action:Event_PromotedToCommander(Handle:event, const String:name[], bool:dontBroadcast) {
	new client = GetClientOfUserId(GetEventInt(event, "userid"));

	if (!IsClientValid(client) || !IsCommanderBanned(client)) {
		return Plugin_Continue;
	}

	ND_DemoteCommander(GetEventInt(event, "teamid"));

	TRS_PrintToChat(client, _, g_use_tr, "%s You're banned from playing as commander.", "%t", "You're banned from playing as commander.", g_Tag);

	return Plugin_Handled;
}

InitialiseSQL() {
	if (InitSQL("cfg/sourcemod/commander_management_nd.cfg", "Config", "commander_management_nd", g_database, g_SQLType)) {
		SetFailState("Cannot establish an SQL connection.");
	}

	SQL_LockDatabase(g_database);
	if (!SQL_FastQuery(g_database, "CREATE TABLE IF NOT EXISTS commander_management_nd ( expiry INTEGER NOT NULL, admin_authid TEXT, name TEXT, authid TEXT PRIMARY KEY )")) {
		decl String:error[256];
		SQL_GetError(g_database, error, sizeof(error));
		SetFailState("Cannot create SQL table: %s.", error);
	}

	decl String:query[256];
	Format(query, sizeof(query), "DELETE FROM commander_management_nd WHERE expiry <= '%i' AND expiry <> 0;", GetTime());
	if (!SQL_FastQuery(g_database, query)) {
		decl String:error[256];
		SQL_GetError(g_database, error, sizeof(error));
	}

	SQL_UnlockDatabase(g_database);
}

CheckCommanderBan(client) {
	if (!IsClientValid(client)) {
		return;
	}

	decl String:authid[32];
	GetClientAuthString(client, authid, sizeof(authid));

	decl String:query[1024];
	Format(query, sizeof(query), "SELECT expiry FROM commander_management_nd WHERE authid = '%s' AND (expiry = 0 OR expiry > '%i');", authid, GetTime());

	SQL_TQuery(g_database, SQLT_CheckCommanderBan, query, GetClientUserId(client));
}

public SQLT_CheckCommanderBan(Handle:owner, Handle:hndl, const String:error[], any:data) {
	if (hndl == INVALID_HANDLE) {
		decl String:buff[512];
		SQL_GetError(owner, buff, sizeof(buff));
		LogError(buff);
		return;
	}

	new client = GetClientOfUserId(data);

	if (!IsClientValid(client)) {
		return;
	}

	if (SQL_FetchRow(hndl)) {
		g_BanTime[client] = SQL_FetchInt(hndl, 0);
		PrintToChatAll("%s %N is banned from playing as commander!", g_Tag, client);
	}
}

CommanderBan(client, admin, time) {
	if (!IsClientValid(client) || g_BanTime[client] == 0 || (time == -2 && g_BanTime[client] >= 0) || (g_BanTime[client] > 0 && g_BanTime[client] >= time)) {
		return false;
	}

	if (time >= 0) {
		decl String:admin_authid[32],
			String:authid[32];

		if (IsClientValid(admin)) {
			GetClientAuthString(admin, admin_authid, sizeof(admin_authid));
		}
		else {
			admin_authid[0] = '\0';
		}

		GetClientAuthString(client, authid, sizeof(authid));
		
		decl String:name[MAX_NAME_LENGTH],
			String:name_escaped[sizeof(name)*2+1];
		GetClientName(client, name, sizeof(name));
		SQL_EscapeString(g_database, name, name_escaped, sizeof(name_escaped));
		
		decl String:query[1024];
		Format(query, sizeof(query), "%s INTO commander_management_nd (expiry, admin_authid, name, authid) VALUES ('%i', '%s', '%s', '%s');", (g_SQLType == SQLType_SQLITE ? "INSERT OR REPLACE" : "REPLACE"), time, admin_authid, name_escaped, authid);

		new Handle:datapack = CreateDataPack();
		WritePackCell(datapack, GetClientUserId(client));
		// WritePackCell(datapack, IsClientValid(admin) ? GetClientUserId(admin) : INVALID_ENT_REFERENCE);
		WritePackCell(datapack, time);
		WritePackString(datapack, authid);

		SQL_TQuery(g_database, SQLT_CommanderBan, query, datapack);
	}

	g_BanTime[client] = time;
	OnCommanderBanned(client);

	return true;
}

public SQLT_CommanderBan(Handle:owner, Handle:hndl, const String:error[], any:datapack) {
	if (hndl == INVALID_HANDLE) {
		ResetPack(datapack);
		new client = GetClientOfUserId(ReadPackCell(datapack));

		if (!IsClientValid(client)) {
			client = INVALID_ENT_REFERENCE;
		}

		decl String:buff[512];
		if (client != INVALID_ENT_REFERENCE) {
			GetClientName(client, buff, sizeof(buff));
		}
		else {
			Format(buff, sizeof(buff), "NAME_UNAVAILABLE");
		}

		new time = ReadPackCell(datapack);

		decl String:authid[32];
		ReadPackString(datapack, authid, sizeof(authid));

		LogError("Could not ban %s (client index %i / authid %s) from commanding for %.1f minutes.", buff, client, authid, time / 60.0);

		SQL_GetError(owner, buff, sizeof(buff));
		LogError(buff);
	}

	CloseHandle(datapack);
}
	
CommanderUnban(client, admin) {
	if (!IsClientValid(client) || g_BanTime[client] == -1) {
		return false;
	}

	if (g_BanTime[client] >= 0) {
		decl String:authid[32];

		GetClientAuthString(admin, authid, sizeof(authid));

		decl String:query[1024];
		Format(query, sizeof(query), "DELETE FROM commander_management_nd WHERE authid = '%s';", authid);

		new Handle:datapack = CreateDataPack();
		WritePackCell(datapack, GetClientUserId(client));
		// WritePackCell(datapack, IsClientValid(admin) ? GetClientUserId(admin) : INVALID_ENT_REFERENCE);
		WritePackString(datapack, authid);

		SQL_TQuery(g_database, SQLT_CommanderUnban, query, datapack);
	}

	g_BanTime[client] = -1;

	return true;
}

public SQLT_CommanderUnban(Handle:owner, Handle:hndl, const String:error[], any:datapack) {
	if (hndl == INVALID_HANDLE || !SQL_GetAffectedRows(hndl)) {
		ResetPack(datapack);
		new client = GetClientOfUserId(ReadPackCell(datapack));

		if (!IsClientValid(client)) {
			client = INVALID_ENT_REFERENCE;
		}

		decl String:buff[512];
		if (client != INVALID_ENT_REFERENCE) {
			GetClientName(client, buff, sizeof(buff));
		}
		else {
			Format(buff, sizeof(buff), "NAME_UNAVAILABLE");
		}

		decl String:authid[32];
		ReadPackString(datapack, authid, sizeof(authid));

		if (hndl != INVALID_HANDLE) {
			LogError("Could not unban %s (client index %i / authid %s) from commanding.", buff, client, authid);
			SQL_GetError(owner, buff, sizeof(buff));
			LogError(buff);
		}
		else {
			LogError("Player %s (client index %i / authid %s) ban was not in the database.", buff, client, authid);
		}
	}

	CloseHandle(datapack);
}

OnCommanderBanned(client) {
	if (!IsClientValid(client)) {
		return;
	}

	new team = GetClientTeam(client);
	if ((team == ND_TEAM_CONSORTIUM || team == ND_TEAM_EMPIRE) && client == ND_GetCommander(team)) {
		ND_DemoteCommander(team);
	}

	FakeClientCommand(client, "unapplyforcommander");
}

bool:IsCommanderBanned(client) {
	if (g_BanTime[client] > 0 && g_BanTime[client] < GetTime()) {
		g_BanTime[client] = -1;
	}

	return g_BanTime[client] != -1;
}

public MenuHandler_Generic(Handle:menu, MenuAction:action, param1, param2) {
	if (action == MenuAction_End) {
		CloseHandle(menu);
	}
}

stock ND_GetCommander(team) {
	if (team != ND_TEAM_CONSORTIUM && team != ND_TEAM_EMPIRE) {
		ThrowError("Invalid team index specified: got %i expected [2,3]", team);
	}

	new ret = GameRules_GetPropEnt("m_hCommanders", team-2);
	
	return IsClientValid(ret) ? ret : INVALID_ENT_REFERENCE;
}

stock ND_DemoteCommander(team) {
	if (team != ND_TEAM_CONSORTIUM && team != ND_TEAM_EMPIRE) {
		ThrowError("Invalid team index specified: got %i expected [2,3]", team);
	}

	new target = ND_GetCommander(team);

	if (target == INVALID_ENT_REFERENCE) {
		return INVALID_ENT_REFERENCE;
	}

	#if defined PROBLEMS
	FakeClientCommand(target, "rtsview");
	GameRules_SetPropEnt("m_hCommanders", -1, team-2, true);
	#else
	FakeClientCommand(target, "rtsview");
	FakeClientCommand(target, "startmutiny");
	#endif // defined PROBLEMS

	return target;
}

stock ND_PromoteToCommander(client) {
	if (!IsClientValid(client)) {
		ThrowError("Invalid client index %i specified.", client);
	}

	new team = GetClientTeam(client);
	if ((team != ND_TEAM_CONSORTIUM && team != ND_TEAM_EMPIRE) || client == ND_GetCommander(team)) {
		return false;
	}

	#if defined PROBLEMS
	ND_DemoteCommander(team);
	FakeClientCommand(client, "applyforcommander");
	GameRules_SetPropEnt("m_hCommanders", client, team-2, true);

	new Handle:event = CreateEvent("promoted_to_commander", true);
	if (event != INVALID_HANDLE) {
		SetEventInt(event, "userid", GetClientUserId(client));
		SetEventInt(event, "teamid", team);
		FireEvent(event);
	}
	#else
	ServerCommand("_promote_to_commander %i", client);
	#endif // defined PROBLEMS

	return true;
}

stock IsClientValid(&client) {
	if (client <= 0 || client > MaxClients) {
		client = GetClientFromSerial(client);
	}

	return client > 0 && client <= MaxClients && IsClientConnected(client) && IsClientInGame(client);
}

stock const String:__g_SQLDriver[_:SQLType + 1][] = {
	"SQLite",
	"MySQL",
	"UNKOWN_SQL_TYPE"
};

stock InitSQL(const String:ConfigFile[], const String:KeyValues[], const String:SQLiteDBName[], &Handle:database, &SQLType:DatabaseType) {
	new ret = 0;

	#if defined LOG_PERFORMANCE
	new Handle:profiler = CreateProfiler();
	StartProfiling(profiler);
	#endif // defined LOG_PERFORMANCE
	if (database != INVALID_HANDLE) {
		CloseHandle2(database);
	}

	decl String:buff[256], String:NamedConfig[128];
	new SQLType:RequestedSQLType = SQLType_SQLITE;
	new Handle:kv = CreateKeyValues(KeyValues);

	GetGameFolderName(buff, sizeof(buff));
	new len = strlen(buff) + strlen(ConfigFile) + 5;
	decl String:Config[len];
	Format(Config, len, "../%s/%s", buff, ConfigFile);

	if (FileExists(Config)) {
		if (!FileToKeyValues(kv, Config)) {
			LogError("Couldn\'t load file '%s' as a keyvalue structure.", Config);
		}
		else {
			if (!KvGotoFirstSubKey(kv)) {
				LogError("Couldn\'t find first subkey in file '%s'.", Config);
			}
			else {
				do {
					KvGetSectionName(kv, buff, sizeof(buff));

					if (StrEqual(buff, "SQL")) {
						if (KvGotoFirstSubKey(kv)) {
							do {
								KvGetSectionName(kv, buff, sizeof(buff));

								if (StrEqual(buff, "Database")) {
									KvGetString(kv, "named_config", NamedConfig, sizeof(NamedConfig), "");

									if (NamedConfig[0] != '\0') {
										RequestedSQLType = SQLType_MYSQL;
									}
								}
							} while (KvGotoNextKey(kv));
						}
					}
				} while (KvGotoNextKey(kv));
			}

			CloseHandle(kv);
		}
	}

	if (RequestedSQLType != SQLType_SQLITE) {
		if (SQL_CheckConfig(NamedConfig)) {
			if ((database = SQL_Connect(NamedConfig, true, buff, sizeof(buff))) == INVALID_HANDLE) {
				LogMessage("Couldn't connect to database using configuration \"%s\": %s", NamedConfig, buff);
				RequestedSQLType = SQLType_SQLITE;
			}
			else {
				DatabaseType = SQLType;

				decl String:str_driver[32];
				new Handle:driver = SQL_ReadDriver(database);

				SQL_GetDriverProduct(driver, str_driver, sizeof(driver));

				if ((DatabaseType = SQLDriverToSQLType(str_driver)) >= SQLType) {
					LogError("Unrecognized database type \"%s\".", driver);
					RequestedSQLType = SQLType_SQLITE;
					CloseHandle2(database);
				}
			}
		}
		else {
			LogMessage("Couldn't find named configuration \"%s\" in databases.cfg.", NamedConfig);
			RequestedSQLType = SQLType_SQLITE;
		}
	}
	if (RequestedSQLType == SQLType_SQLITE) {
		if ((database = SQLite_UseDatabase(SQLiteDBName, buff, sizeof(buff))) == INVALID_HANDLE) {
			LogError(buff);
			#if defined LOG_PERFORMANCE
			CloseHandle(profiler);
			#endif // defined LOG_PERFORMANCE
			return ret + 1;
		}
	}

	#if defined LOG_PERFORMANCE
	StopProfiling(profiler);
	LogMessage("Parsing config file and establishing an SQL connection took %f seconds.", GetProfilerTime(profiler));
	CloseHandle(profiler);
	#endif // defined LOG_PERFORMANCE

	return ret;
}

stock SQLType:SQLDriverToSQLType(const String:driver[]) {
	for (new i = 0; i < _:SQLType; i++) {
		if (StrEqual(driver, __g_SQLDriver[i], false)) {
			return SQLType:i;
		}
	}

	return SQLType;
}

stock CloseHandle2(&Handle:hndl) {
	if (hndl != INVALID_HANDLE) {
		CloseHandle(hndl);
		hndl = INVALID_HANDLE;
	}
}

stock any:InitCvar(&Handle:cvar, ConVarChanged:callback, const String:name[], const String:defaultValue[], const String:description[] = "", flags = 0, bool:hasMin = false, Float:min = 0.0, bool:hasMax = false, Float:max = 0.0, type = -1) {
	cvar = CreateConVar(name, defaultValue, description, flags, hasMin, min, hasMax, max);
	if (cvar != INVALID_HANDLE) {
		HookConVarChange(cvar, callback);
	}
	else {
		LogMessage("Couldn't create console variable \"%s\", using default value \"%s\"", name, defaultValue);
	}

	if (type < 0 || type > 3) {
		type = 1;
		new len = strlen(defaultValue);
		for (new i = 0; i < len; i++) {
			if (defaultValue[i] == '.') {
				type = 2;
			}
			else if (IsCharNumeric(defaultValue[i])) {
				continue;
			}
			else {
				type = 0;
				break;
			}
		}
	}

	if (type == 1) {
		return cvar != INVALID_HANDLE ? GetConVarInt(cvar) : StringToInt(defaultValue);
	}
	else if (type == 2) {
		return cvar != INVALID_HANDLE ? GetConVarFloat(cvar) : StringToFloat(defaultValue);
	}
	else if (cvar != INVALID_HANDLE && type == 3) {
		Call_StartFunction(INVALID_HANDLE, callback);
		Call_PushCell(cvar);
		Call_PushString("");
		Call_PushString(defaultValue);
		Call_Finish();
	}

	return 0;
}

stock TRS_Format(client, bool:use_tr = false, String:buffer[], maxlength, const String:format[], const String:tr_format[], const String:translation[], any:...) {
	SetGlobalTransTarget(IsClientValid(client) ? client : LANG_SERVER);
	VFormat(buffer, maxlength, use_tr ? tr_format : format, use_tr ? 7 : 8);
}

stock TRS_PrintToChat(client, subject = -1, bool:use_tr = false, const String:format[], const String:tr_format[], const String:translation[], any:...) {
	#pragma unused translation
	if (client < 0 || client > MaxClients || (client && (!IsClientConnected(client) || !IsClientInGame(client)))) {
		return;
	}

	if (subject <= 0 || subject > MaxClients || !IsClientConnected(subject) || !IsClientInGame(subject)) {
		subject = -1;
	}

	SetGlobalTransTarget(client ? client : LANG_SERVER);

	decl String:message[512];
	VFormat(message, sizeof(message), use_tr ? tr_format : format, use_tr ? 6 : 7);

	if (client) {
		new Handle:buffer = StartMessageOne(IsSayText2Available() ? "SayText2" : "SayText", client, USERMSG_RELIABLE);
		if (buffer != INVALID_HANDLE) {
			BfWriteByte(buffer, subject);
			if (IsSayText2Available()) {
				BfWriteByte(buffer, true);
			}
			BfWriteString(buffer, message);
			if (!IsSayText2Available()) {
				BfWriteByte(buffer, -1); // DoD: S
			}
			EndMessage();
		}
	}
	else {
		PrintToServer(message);
	}
}

stock TRS_ReplyToCommand(client, subject = -1, bool:use_tr = false, const String:format[], const String:tr_format[], const String:translation[], any:...) {
	#pragma unused translation
	if (client < 0 || client > MaxClients || (client && (!IsClientConnected(client) || !IsClientInGame(client)))) {
		return;
	}

	if (subject <= 0 || subject > MaxClients || !IsClientConnected(subject) || !IsClientInGame(subject)) {
		subject = -1;
	}

	SetGlobalTransTarget(client ? client : LANG_SERVER);

	decl String:message[512];
	VFormat(message, sizeof(message), use_tr ? tr_format : format, use_tr ? 6 : 7);

	if (client) {
		if (GetCmdReplySource() == SM_REPLY_TO_CHAT) {
			new Handle:buffer = StartMessageOne(IsSayText2Available() ? "SayText2" : "SayText", client, USERMSG_RELIABLE);
			if (buffer != INVALID_HANDLE) {
				BfWriteByte(buffer, subject);
				if (IsSayText2Available()) {
					BfWriteByte(buffer, true);
				}
				BfWriteString(buffer, message);
				if (!IsSayText2Available()) {
					BfWriteByte(buffer, -1); // DoD: S
				}
				EndMessage();
			}
		}
		else {
			PrintToConsole(client, message);
		}
	}
	else {
		PrintToServer(message);
	}
}

stock TRS_ShowActivity2(client, const String:tag[], bool:use_tr = false, const String:format[], const String:tr_format[], const String:translation[], any:...) {
	#pragma unused translation
	if (client < 0 || client > MaxClients || (client && (!IsClientConnected(client) || !IsClientInGame(client)))) {
		return;
	}

	SetGlobalTransTarget(client ? client : LANG_SERVER);

	decl String:message[512];
	VFormat(message, sizeof(message), use_tr ? tr_format : format, use_tr ? 6 : 7);

	ShowActivity2(client, tag, message);
}

stock bool:IsSayText2Available() {
	static bool:init = false,
		bool:ret = false;

	if (!init) {
		ret = GetUserMessageId("SayText2") != INVALID_MESSAGE_ID;
		init = true;
	}

	return ret;
}
